var canvas;
var gl;

var cubeVerticesBuffer;
var cubeVerticesColorBuffer;
var cubeVerticesIndexBuffer;
var pointsPositionBuffer;
var cubeRotationX = 0.0;
var cubeRotationY = 0.0;
var cubeRotationZ = 0.0;
var cubeXOffset = 0.0;
var cubeYOffset = 0.0;
var cubeZOffset = 0.0;
var lastCubeUpdateTime = 0;
var xIncValue = 0.2;
var yIncValue = -0.4;
var zIncValue = 0.3;

var cubeTexture;

var mvMatrix;
var shaderProgram;
var lineProgram;
var textureProgram;
var vertexPositionAttribute;
var vertexColorAttribute;
var lvertexPositionAttribute;
var perspectiveMatrix;
var figure;
var j;
var sliderX;
var sliderY;
var sliderZ;
var sliderE;

var tvertexPositionAttribute;
var textureNormalAttribute;

var textureCoordAttribute;
var normalMatrix;
var normalBuffer;

var lightPos = [1.0, 1.0, 5.0];
var ambientColor = [0.1, 0.1, 0.1];
var diffuseColor = [0.5, 0.5, 0.5];
var specColor = [1.0, 1.0, 1.0];
var shininess = 150.0;
var screenGamma = 2.2;

var colors = [
    [1.0, 1.0, 1.0, 1.0],    // Front face: white
    [1.0, 0.0, 0.0, 1.0],    // Back face: red
    [0.0, 1.0, 0.0, 1.0],    // Top face: green
    [0.0, 0.0, 1.0, 1.0],    // Bottom face: blue
    [1.0, 1.0, 0.0, 1.0],    // Right face: yellow
    [1.0, 0.0, 1.0, 1.0],     // Left face: purple
    [0.5, 0.5, 0.5, 1.0]      // Grey?
];


function vec3mul(v, s) {
    return [v[0] * s, v[1] * s, v[2] * s];
}

function inception() {
    canvas = document.getElementById("glcanvas");
    sliderX = document.getElementById("SliderX");
    sliderY = document.getElementById("SliderY");
    sliderZ = document.getElementById("SliderZ");
    sliderE = document.getElementById("SliderE");
    figure = figure4;
    start();
}

function sliders_reset() {
    sliderX.value = 0.0;
    sliderY.value = 0.0;
    sliderZ.value = 0.0;
    sliderE.value = 0.0;
}

var figure1 = {
    "id": "Figure 1",
    "vertices": [
        // Front face
        -0.5, 0.5, 0.5,
        0.5, 0.5, 0.5,
        0.5, 0.0, 0.5,
        0.0, 0.0, 0.5,
        0.0, -0.5, 0.5,
        -0.5, -0.5, 0.5,

        // Back face
        -0.5, 0.5, 0.0, //6
        0.5, 0.5, 0.0,
        0.5, 0.0, 0.0,
        0.0, 0.0, 0.0,
        0.0, -0.5, 0.0,
        -0.5, -0.5, 0.0,

        // Top face
        -0.5, 0.5, 0.0, // 12
        0.5, 0.5, 0.0,
        0.5, 0.5, 0.5,
        -0.5, 0.5, 0.5,

        // Bottom face
        -0.5, -0.5, 0.5, // 16
        0.0, -0.5, 0.5,
        0.0, -0.5, 0.0,
        -0.5, -0.5, 0.0,
        0.0, 0.0, 0.5,
        0.5, 0.0, 0.5,
        0.5, 0.0, 0.0,
        0.0, 0.0, 0.0,

        // Right face
        0.5, 0.5, 0.5, // 24
        0.5, 0.5, 0.0,
        0.5, 0.0, 0.0,
        0.5, 0.0, 0.5,
        0.0, 0.0, 0.5,
        0.0, 0.0, 0.0,
        0.0, -0.5, 0.0,
        0.0, -0.5, 0.5,

        // Left face
        -0.5, 0.5, 0.0, // 32
        -0.5, 0.5, 0.5,
        -0.5, -0.5, 0.5,
        -0.5, -0.5, 0.0
    ],
    "cubeVertexIndices": [
        0, 1, 5, 1, 2, 3, 3, 4, 5, // Front
        6, 7, 11, 7, 8, 9, 9, 10, 11, // Back
        12, 13, 14, 14, 15, 12, // Top
        16, 17, 18, 18, 19, 16, 20, 21, 22, 22, 23, 20, // Bottom
        24, 25, 26, 26, 27, 24, 28, 29, 30, 30, 31, 28, // Right
        32, 33, 34, 34, 35, 32 // Left
    ],
    "t": [0.0, 0.0, -0.5],
    "r": [-90, 0, 0],
    "e": [0.0, 0.0, -1.0],
    "edges": [
        -0.5, 0.5, 0.5,
        0.5, 0.5, 0.5,
        -0.5, 0.5, 0.5,
        -0.5, -0.5, 0.5,
        -0.5, 0.5, 0.5,
        -0.5, 0.5, 0.0
    ],
    "texIndeces": [
        12, 13, 14, 14, 15, 12, // Top
        0, 1, 5, 1, 2, 3, 3, 4, 5, // front
        32, 33, 34, 34, 35, 32 // Left
    ],
    "texMap": [
        //Front face
        1.0, 1.0,
        1.0 / 3.0, 1.0,
        1.0 / 3.0, 0.75,
        2.0 / 3.0, 0.75,
        2.0 / 3.0, 0.5,
        1.0, 0.5,

        //Back face
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,

        //Top face
        0.0, 2 / 3,
        2 / 3, 2 / 3,
        2 / 3, 1.0,
        0.0, 1.0,

        //Bottom face
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,

        //Right face
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,

        //Left face
        1 / 3, 1.0,
        0.0, 1.0,
        0.0, 0.5,
        1 / 3, 0.5
    ]
};

figure1.colors = (
    function () {
        figure1.generatedColors = [];
        figure1.normals = [];

        //Front face - 6 vertices
        for (j = 0; j < 6; j++) {
            figure1.generatedColors = figure1.generatedColors.concat([1.0, 1.0, 1.0, 1.0]);
            figure1.normals = figure1.normals.concat([0.0, 0.0, 1.0]);
        }

        //Back face - 6 vertices
        for (j = 0; j < 6; j++) {
            figure1.generatedColors = figure1.generatedColors.concat([1.0 - 0.05, 1.0 - 0.05, 1.0 - 0.05, 1.0]);
            figure1.normals = figure1.normals.concat([0.0, 0.0, -1.0]);
        }

        //Top face - 4 vertices
        for (j = 0; j < 4; j++) {
            figure1.generatedColors = figure1.generatedColors.concat([1.0 - 0.1, 1.0 - 0.1, 1.0 - 0.1, 1.0]);
            figure1.normals = figure1.normals.concat([0.0, 1.0, 0.0]);
        }

        //Bottom face - 8 vertices
        for (j = 0; j < 8; j++) {
            figure1.generatedColors = figure1.generatedColors.concat([1.0 - 0.15, 1.0 - 0.15, 1.0 - 0.15, 1.0]);
            figure1.normals = figure1.normals.concat([0.0, -1.0, 0.0]);
        }

        //Right face - 4 vertices
        for (j = 0; j < 8; j++) {
            figure1.generatedColors = figure1.generatedColors.concat([1.0 - 0.20, 1.0 - 0.20, 1.0 - 0.20, 1.0]);
            figure1.normals = figure1.normals.concat([1.0, 0.0, 0.0]);
        }

        //Left face - 4 vertices
        for (j = 0; j < 4; j++) {
            figure1.generatedColors = figure1.generatedColors.concat([1.0 - 0.25, 1.0 - 0.25, 1.0 - 0.25, 1.0]);
            figure1.normals = figure1.normals.concat([-1.0, 0.0, 0.0]);
        }
    }
);

figure1.start = (
    function () {
        figure = figure1;
        start();
    }
);

figure1.colors();

var figure2 = {
    "id": "Figure 2",
    "vertices": [
        // Front face
        -0.5, -0.5, 0.5, //0
        -0.0, -0.5, 0.5,
        0.0, 0.0, 0.5,
        1.0, 0.0, 0.5,
        1.0, 0.5, 0.5,
        0.0, 0.5, 0.5,
        -0.5, 0.5, 0.5,


        // Back face
        -0.5, -0.5, 0.0, //7
        -0.0, -0.5, 0.0,
        0.0, 0.0, 0.0,
        1.0, 0.0, 0.0,
        1.0, 0.5, 0.0,
        0.0, 0.5, 0.0,
        -0.5, 0.5, 0.0,

        // Top face
        -0.5, 0.5, 0.0, //14
        -0.5, 0.5, 0.5,
        1.0, 0.5, 0.5,
        1.0, 0.5, 0.0,

        // Bottom face
        -0.5, -0.5, 0.0, //18
        -0.5, -0.5, 0.5,
        0.0, -0.5, 0.5,
        0.0, -0.5, 0.0,
        0.0, 0.0, 0.0,
        1.0, 0.0, 0.0,
        1.0, 0.0, 0.5,
        0.0, 0.0, 0.5,

        // Right face
        0.0, 0.0, 0.5, //26
        0.0, 0.0, 0.0,
        0.0, -0.5, 0.0,
        0.0, -0.5, 0.5,
        1.0, 0.5, 0.5,
        1.0, 0.5, 0.0,
        1.0, 0.0, 0.0,
        1.0, 0.0, 0.5,

        // Left face
        -0.5, 0.5, 0.0, //34
        -0.5, 0.5, 0.5,
        -0.5, -0.5, 0.5,
        -0.5, -0.5, 0.0
    ],
    "cubeVertexIndices": [
        0, 1, 5, 0, 5, 6, 3, 4, 5, 2, 3, 5,   // front 9
        7, 8, 12, 7, 12, 13, 10, 11, 12, 9, 10, 12,    // back 9
        14, 15, 16, 14, 16, 17,   // top 6
        18, 19, 20, 20, 21, 18, 22, 23, 24, 24, 25, 22, // bottom
        26, 27, 28, 28, 29, 26, 30, 31, 32, 32, 33, 30, // right
        34, 35, 36, 36, 37, 34 // left
    ],
    "t": [0.5, 0.0, 0.5],
    "r": [90, 180, 0],
    "e": [0.0, 0.0, 1.0],
    "edges": [
        -0.5, 0.5, 0.5,
        1.0, 0.5, 0.5,
        -0.5, 0.5, 0.0,
        1.0, 0.5, 0.0,
        -0.5, 0.5, 0.5,
        -0.5, 0.5, 0.0,
        1.0, 0.5, 0.5,
        1.0, 0.5, 0.0,
        -0.5, 0.5, 0.5,
        -0.5, -0.5, 0.5,
        -0.5, 0.5, 0.0,
        -0.5, -0.5, 0.0,
        1.0, 0.5, 0.5,
        1.0, 0.0, 0.5,
        1.0, 0.5, 0.0,
        1.0, 0.0, 0.0,
        0.0, 0.0, 0.0,
        0.0, -0.5, 0.0,
        0.0, 0.0, 0.0,
        0.5, 0.0, 0.0
    ],
    "texIndeces": [
        0, 1, 5, 0, 5, 6, 3, 4, 5, 2, 3, 5, // front
        14, 15, 16, 14, 16, 17,   // top 6
        30, 31, 32, 32, 33, 30, // right
        34, 35, 36, 36, 37, 34, // left
        7, 8, 12, 7, 12, 13, 10, 11, 12, 9, 10, 12    // back 9
    ],
    "texMap": [
        //Front face
        0.0, 0.5,
        1.0 / 3.0, 0.5,
        1.0 / 3.0, 0.25,
        1.0, 0.25,
        1.0, 0.0,
        1.0 / 3.0, 0.0,
        0.0, 0.0,

        //Back face
        1.0, 0.5,
        2 / 3, 0.5,
        2 / 3, 0.25,
        0.0, 0.25,
        0.0, 0.0,
        2 / 3, 0.0,
        1.0, 0.0,

        //Top face
        1.0, 1 / 3,
        1.0, 0.0,
        0.0, 0.0,
        0.0, 1 / 3,

        //Bottom face
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,

        //Right face
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        1 / 3, 0.0,
        1 / 3, 0.25,
        0.0, 0.25,

        //Left face
        2 / 3, 0.0,
        1.0, 0.0,
        1.0, 0.5,
        2 / 3, 0.5
    ]
};

figure2.colors = (
    function () {
        figure2.generatedColors = [];
        figure2.normals = [];

        //Front face - 6 vertices
        for (j = 0; j < 7; j++) {
            figure2.generatedColors = figure2.generatedColors.concat([1.0, 0.0, 0.0, 0.0]);
            figure2.normals = figure2.normals.concat([0.0, 0.0, 1.0]);
        }

        //Back face - 6 vertices
        for (j = 0; j < 7; j++) {
            figure2.generatedColors = figure2.generatedColors.concat([1.0 - 0.05, 0.0, 0.0, 1.0]);
            figure2.normals = figure2.normals.concat([0.0, 0.0, -1.0]);
        }

        //Top face - 4 vertices
        for (j = 0; j < 4; j++) {
            figure2.generatedColors = figure2.generatedColors.concat([1.0 - 0.1, 0.0, 0.0, 1.0]);
            figure2.normals = figure2.normals.concat([0.0, 1.0, 0.0]);
        }

        //Bottom face - 8 vertices
        for (j = 0; j < 8; j++) {
            figure2.generatedColors = figure2.generatedColors.concat([1.0 - 0.15, 0.0, 0.0, 1.0]);
            figure2.normals = figure2.normals.concat([0.0, -1.0, 0.0]);
        }

        //Right face - 4 vertices
        for (j = 0; j < 8; j++) {
            figure2.generatedColors = figure2.generatedColors.concat([1.0 - 0.20, 0.0, 0.0, 1.0]);
            figure2.normals = figure2.normals.concat([1.0, 0.0, 0.0]);
        }

        //Left face - 4 vertices
        for (j = 0; j < 4; j++) {
            figure2.generatedColors = figure2.generatedColors.concat([1.0 - 0.25, 0.0, 0.0, 1.0]);
            figure2.normals = figure2.normals.concat([-1.0, 0.0, 0.0]);
        }
    }
);

figure2.colors();

figure2.start = (
    function () {
        figure = figure2;
        start();
    }
);

var figure3 = {
    "id": "Figure 3",
    "vertices": [
        // Front face
        -0.5, -0.5, 0.5, //0
        -0.0, -0.5, 0.5,
        0.0, 0.0, 0.5,
        0.5, 0.0, 0.5,
        0.5, 0.5, 0.5,
        0.0, 0.5, 0.5,
        -0.5, 0.5, 0.5,
        -1.0, 0.5, 0.5,
        -1.0, 0.0, 0.5,
        -0.5, 0.0, 0.5,


        // Back face
        -0.5, -0.5, 0.0, //10
        -0.0, -0.5, 0.0,
        0.0, 0.0, 0.0,
        0.5, 0.0, 0.0,
        0.5, 0.5, 0.0,
        0.0, 0.5, 0.0,
        -0.5, 0.5, 0.0,
        -1.0, 0.5, 0.0,
        -1.0, 0.0, 0.0,
        -0.5, 0.0, 0.0,

        // Top face
        -1.0, 0.5, 0.0, //20
        -1.0, 0.5, 0.5,
        0.5, 0.5, 0.5,
        0.5, 0.5, 0.0,

        // Bottom face
        -1.0, 0.0, 0.0, //24
        -0.5, 0.0, 0.0,
        -0.5, 0.0, 0.5,
        -1.0, 0.0, 0.5,
        -0.5, -0.5, 0.0,
        0.0, -0.5, 0.0,
        0.0, -0.5, 0.5,
        -0.5, -0.5, 0.5,
        0.0, 0.0, 0.0,
        0.5, 0.0, 0.0,
        0.5, 0.0, 0.5,
        0.0, 0.0, 0.5,

        // Right face
        0.5, 0.0, 0.0, //36
        0.5, 0.5, 0.0,
        0.5, 0.5, 0.5,
        0.5, 0.0, 0.5,
        0.0, 0.0, 0.0,
        0.0, 0.0, 0.5,
        0.0, -0.5, 0.5,
        0.0, -0.5, 0.0,

        // Left face
        -1.0, 0.0, 0.0, //44
        -1.0, 0.0, 0.5,
        -1.0, 0.5, 0.5,
        -1.0, 0.5, 0.0,
        -0.5, 0.0, 0.0,
        -0.5, 0.0, 0.5,
        -0.5, -0.5, 0.5,
        -0.5, -0.5, 0.0
    ],
    "cubeVertexIndices": [
        0, 1, 6, 1, 5, 6, 2, 3, 5, 3, 4, 5, 6, 7, 9, 7, 8, 9,   // front 9
        10, 11, 16, 11, 15, 16, 12, 13, 15, 13, 14, 15, 16, 17, 19, 17, 18, 19,    // back 9
        20, 21, 23, 21, 22, 23,   // top 6
        24, 25, 26, 26, 27, 24, 28, 29, 30, 30, 31, 28, 32, 33, 34, 34, 35, 32,  // bottom 18
        36, 37, 38, 38, 39, 36, 40, 41, 42, 42, 43, 40, // right
        44, 45, 46, 46, 47, 44, 48, 49, 50, 50, 51, 48 // left
    ],
    "t": [0.5, -1.0, -0.5],
    "r": [-90, 0, 0],
    "e": [0.0, -1.0, 0.0],
    "edges": [
        -0.5, -0.5, 0.0,
        0.0, -0.5, 0.0,
        -0.5, -0.5, 0.5,
        0.0, -0.5, 0.5,
        0.5, 0.5, 0.0,
        -1.0, 0.5, 0.0,
        -1.0, 0.5, 0.0,
        -1.0, 0.0, 0.0,
        0.5, 0.5, 0.0,
        0.5, 0.0, 0.0,
        0.5, 0.5, 0.5,
        0.5, 0.5, 0.0,
        -1.0, 0.5, 0.0,
        -1.0, 0.5, 0.5
    ],
    "texIndeces": [
        20, 21, 23, 21, 22, 23,   // top 6
        10, 11, 16, 11, 15, 16, 12, 13, 15, 13, 14, 15, 16, 17, 19, 17, 18, 19,    // back 9
        44, 45, 46, 46, 47, 44, //left
        36, 37, 38, 38, 39, 36, //right
        28, 29, 30, 30, 31, 28 //bottom
    ],
    "texMap": [
        //Front face
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,

        //Back face
        1 / 3, 0.5,
        2 / 3, 0.5,
        2 / 3, 0.75,
        1.0, 0.75,
        1.0, 1.0,
        2 / 3, 1.0,
        1 / 3, 1.0,
        0.0, 1.0,
        0.0, 0.75,
        1 / 3, 0.75,

        //Top face
        0.0, 0.0,
        0.0, 1 / 3,
        1.0, 1 / 3,
        1.0, 0.0,

        //Bottom face
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        1 / 3, 1.0,
        2 / 3, 1.0,
        2 / 3, 2 / 3,
        1 / 3, 2 / 3,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,

        //Right face
        0.0, 0.75,
        0.0, 1.0,
        1 / 3, 1.0,
        1 / 3, 0.75,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,

        //Left face
        1.0, 0.75,
        2 / 3, 0.75,
        2 / 3, 1.0,
        1.0, 1.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0
    ]
};

figure3.colors = (
    function () {
        figure3.generatedColors = [];
        figure3.normals = [];

        //Front face - 6 vertices
        for (j = 0; j < 10; j++) {
            figure3.generatedColors = figure3.generatedColors.concat([0.0, 1.0, 0.0, 1.0]);
            figure3.normals = figure3.normals.concat([0.0, 0.0, 1.0]);
        }

        //Back face - 6 vertices
        for (j = 0; j < 10; j++) {
            figure3.generatedColors = figure3.generatedColors.concat([0.0, 1.0 - 0.05, 0.0, 1.0]);
            figure3.normals = figure3.normals.concat([0.0, 0.0, -1.0]);
        }

        //Top face - 4 vertices
        for (j = 0; j < 4; j++) {
            figure3.generatedColors = figure3.generatedColors.concat([0.0, 1.0 - 0.10, 0.0, 1.0]);
            figure3.normals = figure3.normals.concat([0.0, 1.0, 0.0]);
        }

        //Bottom face - 8 vertices
        for (j = 0; j < 12; j++) {
            figure3.generatedColors = figure3.generatedColors.concat([0.0, 1.0 - 0.15, 0.0, 1.0]);
            figure3.normals = figure3.normals.concat([0.0, -1.0, 0.0]);
        }

        //Right face - 4 vertices
        for (j = 0; j < 8; j++) {
            figure3.generatedColors = figure3.generatedColors.concat([0.0, 1.0 - 0.20, 0.0, 1.0]);
            figure3.normals = figure3.normals.concat([1.0, 0.0, 0.0]);
        }

        //Left face - 4 vertices
        for (j = 0; j < 8; j++) {
            figure3.generatedColors = figure3.generatedColors.concat([0.0, 1.0 - 0.25, 0.0, 1.0]);
            figure3.normals = figure3.normals.concat([-1.0, 0.0, 0.0]);
        }
    }
);

figure3.colors();

figure3.start = (
    function () {
        figure = figure3;
        start();
    }
);

figure4 = {
    "id": "Figure 4",
    "vertices": [
        // Front face
        -0.5, 0.0, 0.5, //0
        0.0, 0.0, 0.5,
        0.0, 0.5, 0.5,
        -0.5, 0.5, 0.5,
        0.0, 0.0, 0.0,
        0.5, 0.0, 0.0,
        0.5, 0.5, 0.0,
        0.0, 0.5, 0.0,

        // Back face
        -0.5, 0.0, -0.5, //8
        0.0, 0.0, -0.5,
        0.0, 0.5, -0.5,
        -0.5, 0.5, -0.5,
        0.0, 0.0, -1.0,
        0.5, 0.0, -1.0,
        0.5, 0.5, -1.0,
        0.0, 0.5, -1.0,

        // Top face
        -0.5, 0.5, -0.5, //16
        -0.5, 0.5, 0.5,
        0.0, 0.5, 0.5,
        0.0, 0.5, 0.0,
        0.5, 0.5, 0.0,
        0.5, 0.5, -0.5,
        0.5, 0.5, -1.0,
        0.0, 0.5, -1.0,
        0.0, 0.5, -0.5,

        // Bottom face
        -0.5, 0.0, -0.5, //25
        -0.5, 0.0, 0.5,
        0.0, 0.0, 0.5,
        0.0, 0.0, 0.0,
        0.5, 0.0, 0.0,
        0.5, 0.0, -0.5,
        0.5, 0.0, -1.0,
        0.0, 0.0, -1.0,
        0.0, 0.0, -0.5,

        //Right Face
        0.0, 0.0, 0.5,//34
        0.0, 0.5, 0.5,
        0.0, 0.5, 0.0,
        0.0, 0.0, 0.0,
        0.5, 0.0, 0.0,
        0.5, 0.5, 0.0,
        0.5, 0.5, -1.0,
        0.5, 0.0, -1.0,

        //Left Face
        -0.5, 0.0, 0.5,//34
        -0.5, 0.5, 0.5,
        -0.5, 0.5, -0.5,
        -0.5, 0.0, -0.5,
        0.0, 0.0, -0.5,
        0.0, 0.5, -0.5,
        0.0, 0.5, -1.0,
        0.0, 0.0, -1.0

    ],
    "cubeVertexIndices": [
        0, 1, 2, 0, 2, 3, 4, 5, 6, 4, 6, 7, // Front Face
        8, 9, 10, 8, 10, 11, 12, 13, 14, 12, 14, 15, // Back Face
        16, 17, 21, 17, 18, 19, 19, 20, 21, 22, 23, 24, 21, 22, 24, // Top Face
        25, 26, 30, 26, 27, 28, 28, 29, 30, 31, 32, 33, 30, 31, 33, // Bottom Face
        34, 35, 37, 36, 37, 35, 38, 39, 41, 40, 41, 39,
        42, 43, 45, 44, 45, 43, 46, 47, 49, 48, 49, 47
    ],
    "t": [0.0, -0.5, 0.0],
    "r": [0, 0, 90],
    "e": [-1.0, 0.0, 0.0],
    "edges": [
        -0.5, 0.5, 0.5,
        0.0, 0.5, 0.5,

        -0.5, 0.0, 0.5,
        0.0, 0.0, 0.5,

        -0.5, 0.5, 0.5,
        -0.5, 0.0, 0.5,

        0.0, 0.5, 0.5,
        0.0, 0.0, 0.5,

        -0.5, 0.5, 0.5,
        -0.5, 0.5, -0.5,

        -0.5, 0.0, 0.5,
        -0.5, 0.0, 0.0,

        0.0, 0.5, 0.5,
        0.0, 0.5, 0.0,

        0.5, 0.5, 0.0,
        0.0, 0.5, 0.0,

        0.0, 0.5, -1.0,
        0.5, 0.5, -1.0
    ],
    "texIndeces": [
        12, 13, 14, 12, 14, 15, // Back Face
        0, 1, 2, 0, 2, 3, 4, 5, 6, 4, 6, 7, // Front Face
        16, 17, 21, 17, 18, 19, 19, 20, 21, 22, 23, 24, 21, 22, 24, // Top Face
        42, 43, 45, 44, 45, 43 //left
    ],
    "texMap": [
        //Front face
        1 / 3, 1.0,
        1 / 3, 2 / 3,
        0.0, 2 / 3,
        0.0, 1.0,
        1 / 3, 2 / 3,
        1 / 3, 1 / 3,
        0.0, 1 / 3,
        0.0, 2 / 3,

        //Back face
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        1 / 3, 1 / 3,
        1 / 3, 2 / 3,
        0.0, 2 / 3,
        0.0, 1 / 3,

        //Top face
        1.0, 0.75,
        1.0, 0.25,
        2 / 3, 0.25,
        2 / 3, 0.5,
        1 / 3, 0.5,
        1 / 3, 0.75,
        1 / 3, 1.0,
        2 / 3, 1.0,
        2 / 3, 0.75,

        //Bottom face
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,

        //Right face
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,

        //Left face
        1 / 3, 0.25,
        0.0, 0.25,
        0.0, 0.75,
        1 / 3, 0.75,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0
    ]
};

figure4.colors = (
    function () {
        figure4.generatedColors = [];
        figure4.normals = [];

        //Front face - 6 vertices
        for (j = 0; j < 8; j++) {
            figure4.generatedColors = figure4.generatedColors.concat([0.0, 0.0, 1.0, 1.0]);
            figure4.normals = figure4.normals.concat([0.0, 0.0, 1.0]);
        }

        //Back face - 6 vertices
        for (j = 0; j < 8; j++) {
            figure4.generatedColors = figure4.generatedColors.concat([0.0, 0.0, 1.0 - 0.05, 1.0]);
            figure4.normals = figure4.normals.concat([0.0, 0.0, -1.0]);
        }

        //Top face - 6 vertices
        for (j = 0; j < 9; j++) {
            figure4.generatedColors = figure4.generatedColors.concat([0.0, 0.0, 1.0 - 0.1, 1.0]);
            figure4.normals = figure4.normals.concat([0.0, 1.0, 0.0]);
        }

        //Bottom face - 6 vertices
        for (j = 0; j < 9; j++) {
            figure4.generatedColors = figure4.generatedColors.concat([0.0, 0.0, 1.0 - 0.15, 1.0]);
            figure4.normals = figure4.normals.concat([0.0, -1.0, 0.0]);
        }
        //Right face - 6 vertices
        for (j = 0; j < 8; j++) {
            figure4.generatedColors = figure4.generatedColors.concat([0.0, 0.0, 1.0 - 0.2, 1.0]);
            figure4.normals = figure4.normals.concat([1.0, 0.0, 0.0]);
        }
        //Left face - 6 vertices
        for (j = 0; j < 8; j++) {
            figure4.generatedColors = figure4.generatedColors.concat([0.0, 0.0, 1.0 - 0.25, 1.0]);
            figure4.normals = figure4.normals.concat([-1.0, 0.0, 0.0]);
        }
    }
);

figure4.colors();

figure4.start = (
    function () {
        figure = figure4;
        start();
    }
);

figure5 = {
    "id": "Figure 5",
    "vertices": [
        // Front face
        0.0, 0.0, 0.5, //0
        0.5, 0.0, 0.5,
        0.5, 0.5, 0.5,
        0.0, 0.5, 0.5,
        -0.5, -0.5, 0.0,
        0.5, -0.5, 0.0,
        0.5, 0.0, 0.0,
        -0.5, 0.0, 0.0,

        // Back face
        0.0, 0.0, -0.5, //8
        0.5, 0.0, -0.5,
        0.5, 0.5, -0.5,
        0.0, 0.5, -0.5,
        -0.5, -0.5, -0.5,
        0.5, -0.5, -0.5,
        0.5, 0.0, -0.5,
        -0.5, 0.0, -0.5,

        // Top face
        0.0, 0.5, 0.5, //16
        0.5, 0.5, 0.5,
        0.5, 0.5, -0.5,
        0.0, 0.5, -0.5,
        -0.5, 0.0, 0.0,
        0.0, 0.0, 0.0,
        0.0, 0.0, -0.5,
        -0.5, 0.0, -0.5,

        // Bottom face
        0.0, 0.0, 0.0, //24
        0.5, 0.0, 0.0,
        0.5, 0.0, 0.5,
        0.0, 0.0, 0.5,
        -0.5, -0.5, -0.5,
        0.5, -0.5, -0.5,
        0.5, -0.5, 0.0,
        -0.5, -0.5, 0.0,

        //Right Face
        0.5, 0.0, 0.5,//32
        0.5, 0.0, 0.0,
        0.5, -0.5, 0.0,
        0.5, -0.5, -0.5,
        0.5, 0.5, -0.5,
        0.5, 0.5, 0.5,

        //Left Face
        -0.5, -0.5, -0.5,//38
        -0.5, -0.5, 0.0,
        -0.5, 0.0, 0.0,
        -0.5, 0.0, -0.5,
        0.0, 0.0, -0.5,
        0.0, 0.0, 0.5,
        0.0, 0.5, 0.5,
        0.0, 0.5, -0.5

    ],
    "cubeVertexIndices": [
        0, 1, 2, 0, 2, 3, 4, 5, 6, 4, 6, 7, // Front Face
        8, 9, 10, 8, 10, 11, 12, 13, 14, 12, 14, 15, // Back Face
        16, 17, 18, 16, 18, 19, 20, 21, 22, 20, 22, 23, // Top Face
        24, 25, 26, 24, 26, 27, 28, 29, 30, 28, 30, 31, // Bottom Face
        37, 35, 36, 32, 33, 37, 33, 34, 35, // Right Face
        38, 39, 40, 38, 40, 41, 42, 43, 44, 42, 44, 45 // Left Face
    ],
    "t": [0.5, 0.0, -0.5],
    "r": [0.0, 0.0, 0.0],
    "e": [1.0, 0.0, -1.0],
    "edges":[
        0.5, 0.5, -0.5,
        0.5, 0.5, 0.5,

        0.5, 0.5, -0.5,
        0.0, 0.5, -0.5,

        0.5, 0.5, -0.5,
        0.5, -0.5, -0.5
    ],
    "texIndeces": [
        8, 9, 10, 8, 10, 11, 12, 13, 14, 12, 14, 15, // Back Face
        16, 17, 18, 16, 18, 19, //top
        37, 35, 36, 32, 33, 37, 33, 34, 35 // Right Face
    ],
    "texMap": [
        //Front face
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,

        //Back face
        2 / 3, 2 / 3,
        1.0, 2 / 3,
        1.0, 1.0,
        2 / 3, 1.0,
        1 / 3, 1 / 3,
        1.0, 1 / 3,
        1.0, 2 / 3,
        1 / 3, 2 / 3,

        //Top face
        1.0 / 3.0, 0.5,
        0.0, 0.5,
        0.0, 1.0,
        1.0 / 3.0, 1.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,

        //Bottom face
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,

        //Right face
        2 / 3, 0.5,
        2 / 3, 0.75,
        1 / 3, 0.75,
        1 / 3, 1.0,
        1.0, 1.0,
        1.0, 0.5,

        //Left face
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0
    ]
};

figure5.colors = (
    function () {
        figure5.generatedColors = [];
        figure5.normals = [];

        //Front face - 6 vertices
        for (j = 0; j < 8; j++) {
            figure5.generatedColors = figure5.generatedColors.concat([1.0, 1.0, 0.0, 1.0]);
            figure5.normals = figure5.normals.concat([0.0, 0.0, 1.0]);
        }

        //Back face - 6 vertices
        for (j = 0; j < 8; j++) {
            figure5.generatedColors = figure5.generatedColors.concat([1.0 - 0.05, 1.0 - 0.05, 0.0, 1.0]);
            figure5.normals = figure5.normals.concat([0.0, 0.0, -1.0]);
        }

        //Top face - 6 vertices
        for (j = 0; j < 8; j++) {
            figure5.generatedColors = figure5.generatedColors.concat([1.0 - 0.10, 1.0 - 0.10, 0.0, 1.0]);
            figure5.normals = figure5.normals.concat([0.0, 1.0, 0.0]);
        }

        //Bottom face - 6 vertices
        for (j = 0; j < 8; j++) {
            figure5.generatedColors = figure5.generatedColors.concat([1.0 - 0.15, 1.0 - 0.15, 0.0, 1.0]);
            figure5.normals = figure5.normals.concat([0.0, -1.0, 0.0]);
        }
        //Right face - 6 vertices
        for (j = 0; j < 6; j++) {
            figure5.generatedColors = figure5.generatedColors.concat([1.0 - 0.20, 1.0 - 0.20, 0.0, 1.0]);
            figure5.normals = figure5.normals.concat([1.0, 0.0, 0.0]);
        }
        //Left face - 6 vertices
        for (j = 0; j < 8; j++) {
            figure5.generatedColors = figure5.generatedColors.concat([1.0 - 0.25, 1.0 - 0.25, 0.0, 1.0]);
            figure5.normals = figure5.normals.concat([-1.0, 0.0, 0.0]);
        }
    }
);

figure5.colors();

figure5.start = (
    function () {
        figure = figure5;
        start();
    }
);

figure6 = {
    "id": "Figure 6",
    "vertices": [
        // Front face
        0.0, -0.5, 0.5, //0
        0.5, -0.5, 0.5,
        0.5, 0.0, 0.5,
        0.0, 0.0, 0.5,
        -0.5, 0.0, 0.0,
        0.5, 0.0, 0.0,
        0.5, 0.5, 0.0,
        -0.5, 0.5, 0.0,

        // Back face
        0.0, 0.0, -0.5, //8
        0.0, -0.5, -0.5,
        0.5, -0.5, -0.5,
        0.5, 0.0, -0.5,
        0.5, 0.5, -0.5,
        -0.5, 0.5, -0.5,
        -0.5, 0.0, -0.5,

        // Top face
        -0.5, 0.5, 0.0, //15
        0.5, 0.5, 0.0,
        0.5, 0.5, -0.5,
        -0.5, 0.5, -0.5,
        0.0, 0.0, 0.5,
        0.5, 0.0, 0.5,
        0.5, 0.0, 0.0,
        0.0, 0.0, 0.0,

        // Bottom face
        0.0, -0.5, 0.5, //23
        0.5, -0.5, 0.5,
        0.5, -0.5, -0.5,
        0.0, -0.5, -0.5,
        -0.5, 0.0, 0.0,
        0.0, 0.0, 0.0,
        0.0, 0.0, -0.5,
        -0.5, 0.0, -0.5,

        //Right Face
        0.5, -0.5, 0.5,//31
        0.5, -0.5, -0.5,
        0.5, 0.5, -0.5,
        0.5, 0.5, 0.0,
        0.5, 0.0, 0.0,
        0.5, 0.0, 0.5,

        //Left Face
        -0.5, 0.0, -0.5,//37
        -0.5, 0.0, 0.0,
        -0.5, 0.5, 0.0,
        -0.5, 0.5, -0.5,
        0.0, -0.5, -0.5,
        0.0, -0.5, 0.5,
        0.0, 0.0, 0.5,
        0.0, 0.0, -0.5

    ],
    "cubeVertexIndices": [
        0, 1, 2, 0, 2, 3, 4, 5, 6, 4, 6, 7, // Front Face
        8, 9, 11, 9, 10, 11, 11, 12, 13, 11, 13, 14, // Back Face
        15, 16, 17, 15, 17, 18, 19, 20, 21, 19, 21, 22, // Top Face
        23, 24, 25, 23, 25, 26, 27, 28, 29, 27, 29, 30, // Bottom Face
        31, 32, 33, 33, 34, 35, 35, 36, 31, // Right Face
        37, 38, 39, 37, 39, 40, 41, 42, 43, 41, 43, 44 // Left Face
    ],
    "t": [0.5, -0.5, 0.0],
    "r": [0, 0, 0],
    "e": [1.0, 0.0, 0.0],
    "edges": [
        0.0, 0.0, 0.5,
        0.5, 0.0, 0.5,

        0.0, 0.0, 0.5,
        0.0, -0.5, 0.5,

        0.5, 0.0, 0.5,
        0.5, -0.5, 0.5,

        0.0, -0.5, 0.5,
        0.5, -0.5, 0.5,

        0.0, -0.5, 0.5,
        0.0, -0.5, 0.0,

        0.5, -0.5, 0.5,
        0.5, -0.5, -0.5,

        0.5, 0.0, 0.5,
        0.5, 0.0, 0.0,

        0.5, 0.0, 0.0,
        0.5, 0.5, 0.0
    ],
    "texIndeces": [
        0, 1, 2, 0, 2, 3, 4, 5, 6, 4, 6, 7, // Front Face
        31, 32, 33, 33, 34, 35, 35, 36, 31, // Right Face
        23, 24, 25, 23, 25, 26 //Bottom face
    ],
    "texMap": [
        //Front face
        2 / 3, 1.0,
        1.0, 1.0,
        1.0, 2 / 3,
        2 / 3, 2 / 3,
        1 / 3, 2 / 3,
        1.0, 2 / 3,
        1.0, 1 / 3,
        1 / 3, 1 / 3,

        //Back face
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,

        //Top face
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,

        //Bottom face
        2 / 3, 0.25,
        1.0, 0.25,
        1.0, 0.75,
        2 / 3, 0.75,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,

        //Right face
        0.0, 0.25,
        0.0, 0.75,
        2 / 3, 0.75,
        2 / 3, 0.5,
        1 / 3, 0.5,
        1 / 3, 0.25,

        //Left face
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0
    ]
};

figure6.colors = (
    function () {
        figure6.generatedColors = [];
        figure6.normals = [];

        //Front face - 6 vertices
        for (j = 0; j < 8; j++) {
            figure6.generatedColors = figure6.generatedColors.concat([1.0, 0.0, 1.0, 1.0]);
            figure6.normals = figure6.normals.concat([0.0, 0.0, 1.0]);
        }

        //Back face - 6 vertices
        for (j = 0; j < 7; j++) {
            figure6.generatedColors = figure6.generatedColors.concat([1.0 - 0.05, 0.0, 1.0 - 0.05, 1.0]);
            figure6.normals = figure6.normals.concat([0.0, 0.0, -1.0]);
        }

        //Top face - 6 vertices
        for (j = 0; j < 8; j++) {
            figure6.generatedColors = figure6.generatedColors.concat([1.0 - 0.10, 0.0, 1.0 - 0.10, 1.0]);
            figure6.normals = figure6.normals.concat([0.0, 1.0, 0.0]);
        }

        //Bottom face - 6 vertices
        for (j = 0; j < 8; j++) {
            figure6.generatedColors = figure6.generatedColors.concat([1.0 - 0.15, 0.0, 1.0 - 0.15, 1.0]);
            figure6.normals = figure6.normals.concat([0.0, -1.0, 0.0]);
        }
        //Right face - 6 vertices
        for (j = 0; j < 6; j++) {
            figure6.generatedColors = figure6.generatedColors.concat([1.0 - 0.20, 0.0, 1.0 - 0.20, 1.0]);
            figure6.normals = figure6.normals.concat([1.0, 0.0, 0.0]);
        }
        //Left face - 6 vertices
        for (j = 0; j < 8; j++) {
            figure6.generatedColors = figure6.generatedColors.concat([1.0 - 0.25, 0.0, 1.0 - 0.25, 1.0]);
            figure6.normals = figure6.normals.concat([-1.0, 0.0, 0.0]);
        }
    }
);

figure6.colors();

figure6.start = (
    function () {
        figure = figure6;
        start();
    }
);

figure7 = {
    "id": "Figure 7",
    "vertices": [
        // Front face
        0.0, 0.0, 0.5, //0
        0.5, 0.0, 0.5,
        0.5, 0.5, 0.5,
        0.0, 0.5, 0.5,
        0.0, -0.5, 0.0,
        0.5, -0.5, 0.0,
        0.5, 0.0, 0.0,
        0.0, 0.0, 0.0,
        -0.5, 0.0, 0.0,
        0.0, 0.0, 0.0,
        0.0, 0.5, 0.0,
        -0.5, 0.5, 0.0,

        // Back face
        0.0, 0.0, -0.5, //12
        0.0, -0.5, -0.5,
        0.5, -0.5, -0.5,
        0.5, 0.5, -0.5,
        -0.5, 0.5, -0.5,
        -0.5, 0.0, -0.5,

        // Top face
        0.0, 0.5, 0.0, //18
        0.0, 0.5, 0.5,
        0.5, 0.5, 0.5,
        0.5, 0.5, -0.5,
        -0.5, 0.5, -0.5,
        -0.5, 0.5, 0.0,

        // Bottom face
        0.0, 0.0, 0.0, //24
        0.5, 0.0, 0.0,
        0.5, 0.0, 0.5,
        0.0, 0.0, 0.5,
        0.0, -0.5, -0.5,
        0.5, -0.5, -0.5,
        0.5, -0.5, 0.0,
        0.0, -0.5, 0.0,
        -0.5, 0.0, -0.5,
        0.0, 0.0, -0.5,
        0.0, 0.0, 0.0,
        -0.5, 0.0, 0.0,


        //Right Face
        0.5, 0.0, 0.0,//36
        0.5, -0.5, 0.0,
        0.5, -0.5, -0.5,
        0.5, 0.5, -0.5,
        0.5, 0.5, 0.5,
        0.5, 0.0, 0.5,

        //Left Face
        -0.5, 0.0, -0.5,//42
        -0.5, 0.0, 0.0,
        -0.5, 0.5, 0.0,
        -0.5, 0.5, -0.5,
        0.0, 0.0, 0.0,
        0.0, 0.0, 0.5,
        0.0, 0.5, 0.5,
        0.0, 0.5, 0.0,
        0.0, -0.5, -0.5,
        0.0, -0.5, 0.0,
        0.0, 0.0, 0.0,
        0.0, 0.5, -0.5
    ],
    "cubeVertexIndices": [
        0, 1, 2, 0, 2, 3, 4, 5, 6, 4, 6, 7, 8, 9, 10, 8, 10, 11, // Front Face
        12, 13, 14, 14, 15, 16, 16, 17, 12, // Back Face
        18, 19, 20, 20, 21, 22, 22, 23, 18, // Top Face
        24, 25, 26, 24, 26, 27, 28, 29, 30, 28, 30, 31, 32, 33, 34, 32, 34, 35, // Bottom Face
        36, 37, 38, 38, 39, 40, 40, 41, 36, // Right Face
        42, 43, 44, 42, 44, 45, 46, 47, 48, 46, 48, 49, 50, 51, 52, 50, 52, 53 // Left Face
    ],
    "t": [0.0, 0.0, 0.0],
    "r": [0, -90, 0],
    "e": [0.0, 1.0, 0.0],
    "edges": [
        0.5, 0.0, 0.0,
        0.5, 0.0, -0.5,
        0.5, -0.5, 0.0,
        0.5, -0.5, -0.5,

        0.0, 0.5, 0.5,
        0.5, 0.5, 0.5,

        0.0, 0.0, 0.5,
        0.5, 0.0, 0.5,

        0.5, 0.0, 0.0,
        0.5, -0.5, 0.0,

        0.5, 0.0, -0.5,
        0.5, -0.5, -0.5
    ],
    "texIndeces": [
        18, 19, 20, 20, 21, 22, 22, 23, 18, // top
        36, 37, 38, 38, 39, 40, 40, 41, 36, // Right Face
        12, 13, 14, 14, 15, 16, 16, 17, 12, // Back Face
        0, 1, 2, 0, 2, 3, 4, 5, 6, 4, 6, 7, //Front
        28, 29, 30, 28, 30, 31 //bottom
    ],
    "texMap": [
        //Front face
        1 / 3, 0.5,
        1 / 3, 0.25,
        0.0, 0.25,
        0.0, 0.5,
        2 / 3, 0.5,
        2 / 3, 0.25,
        1 / 3, 0.25,
        1 / 3, 0.5,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,

        //Back face
        2 / 3, 0.5,
        1 / 3, 0.5,
        1 / 3, 0.25,
        1.0, 0.25,
        1.0, 0.75,
        2 / 3, 0.75,

        //Top face
        2.0 / 3.0, 0.5,
        1.0, 0.5,
        1.0, 0.25,
        1.0 / 3.0, 0.25,
        1.0 / 3.0, 0.75,
        2.0 / 3.0, 0.75,

        //Bottom face
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        2 / 3, 0.5,
        2 / 3, 0.25,
        1 / 3, 0.25,
        1 / 3, 0.5,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,

        //Right face
        1 / 3, 1 / 3,
        1 / 3, 2 / 3,
        2 / 3, 2 / 3,
        2 / 3, 0.0,
        0.0, 0.0,
        0.0, 1 / 3,

        //Left face
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0,
        0.0, 0.0
    ]
};

figure7.colors = (
    function () {
        figure7.generatedColors = [];
        figure7.normals = [];

        //Front face - 6 vertices
        for (j = 0; j < 12; j++) {
            figure7.generatedColors = figure7.generatedColors.concat([0.6, 0.6, 0.6, 1.0]);
            figure7.normals = figure7.normals.concat([0.0, 0.0, 1.0]);
        }

        //Back face - 6 vertices
        for (j = 0; j < 6; j++) {
            figure7.generatedColors = figure7.generatedColors.concat([0.5 - 0.05, 0.5 - 0.05, 0.5 - 0.05, 1.0]);
            figure7.normals = figure7.normals.concat([0.0, 0.0, -1.0]);
        }

        //Top face - 6 vertices
        for (j = 0; j < 6; j++) {
            figure7.generatedColors = figure7.generatedColors.concat([0.5 - 0.10, 0.5 - 0.10, 0.5 - 0.10, 1.0]);
            figure7.normals = figure7.normals.concat([0.0, 1.0, 0.0]);
        }

        //Bottom face - 6 vertices
        for (j = 0; j < 12; j++) {
            figure7.generatedColors = figure7.generatedColors.concat([0.5 - 0.15, 0.5 - 0.15, 0.5 - 0.15, 1.0]);
            figure7.normals = figure7.normals.concat([0.0, -1.0, 0.0]);
        }
        //Right face - 6 vertices
        for (j = 0; j < 6; j++) {
            figure7.generatedColors = figure7.generatedColors.concat([0.5 - 0.20, 0.5 - 0.20, 0.5 - 0.20, 1.0]);
            figure7.normals = figure7.normals.concat([1.0, 0.0, 0.0]);
        }
        //Left face - 6 vertices
        for (j = 0; j < 12; j++) {
            figure7.generatedColors = figure7.generatedColors.concat([0.5 - 0.25, 0.5 - 0.25, 0.5 - 0.25, 1.0]);
            figure7.normals = figure7.normals.concat([-1.0, 0.0, 0.0]);
        }
    }
);

figure7.colors();

figure7.start = (
    function () {
        figure = figure7;
        start();
    }
);

var figures = [
    figure1,
    figure2,
    figure3,
    figure4,
    figure5,
    figure6,
    figure7
];

//
// start
//
// Called when the canvas is created to get the ball rolling.
//
function start() {
    /* canvas = document.getElementById("glcanvas");
     sliderX = document.getElementById("SliderX");
     sliderY = document.getElementById("SliderY");
     sliderZ = document.getElementById("SliderZ"); */

    initWebGL(canvas);      // Initialize the GL context

    // Only continue if WebGL is available and working

    if (gl) {
        gl.clearColor(0.0, 0.0, 0.0, 1.0);  // Clear to black, fully opaque
        gl.clearDepth(1.0);                 // Clear everything
        gl.enable(gl.DEPTH_TEST);           // Enable depth testing
        gl.depthFunc(gl.LEQUAL);            // Near things obscure far things

        // Initialize the shaders; this is where all the lighting for the
        // vertices and so forth is established.

        initShaders();

        // Here's where we call the routine that builds all the objects
        // we'll be drawing.

        initBuffers();

        // Set up to draw the scene periodically.
        requestAnimationFrame(drawScene);
        sliderX.oninput = (function () {
            requestAnimationFrame(drawScene);
            document.getElementById("rotateXvalue").innerHTML = sliderX.value.toString();
            document.getElementById("rotateYvalue").innerHTML = sliderY.value.toString();
            document.getElementById("rotateZvalue").innerHTML = sliderZ.value.toString();
        });
        sliderY.oninput = (function () {
            requestAnimationFrame(drawScene);
            document.getElementById("rotateXvalue").innerHTML = sliderX.value.toString();
            document.getElementById("rotateYvalue").innerHTML = sliderY.value.toString();
            document.getElementById("rotateZvalue").innerHTML = sliderZ.value.toString();
        });
        sliderZ.oninput = (function () {
            requestAnimationFrame(drawScene);
            document.getElementById("rotateXvalue").innerHTML = sliderX.value.toString();
            document.getElementById("rotateYvalue").innerHTML = sliderY.value.toString();
            document.getElementById("rotateZvalue").innerHTML = sliderZ.value.toString();
        });
        sliderE.oninput = null;
    }
}

function startAll() {
    /* canvas = document.getElementById("glcanvas");
     sliderX = document.getElementById("SliderX");
     sliderY = document.getElementById("SliderY");
     sliderZ = document.getElementById("SliderZ"); */

    initWebGL(canvas);      // Initialize the GL context

    // Only continue if WebGL is available and working

    if (gl) {
        gl.clearColor(0.0, 0.0, 0.0, 1.0);  // Clear to black, fully opaque
        gl.clearDepth(1.0);                 // Clear everything
        gl.enable(gl.DEPTH_TEST);           // Enable depth testing
        gl.depthFunc(gl.LEQUAL);            // Near things obscure far things

        // Initialize the shaders; this is where all the lighting for the
        // vertices and so forth is established.

        initShaders();

        // Here's where we call the routine that builds all the objects
        // we'll be drawing.

        initBuffersAll();
        initTextures();

        // Set up to draw the scene periodically.
        requestAnimationFrame(drawSceneAll);
        sliderX.oninput = (function () {
            requestAnimationFrame(drawSceneAll);
            document.getElementById("rotateXvalue").innerHTML = sliderX.value.toString();
            document.getElementById("rotateYvalue").innerHTML = sliderY.value.toString();
            document.getElementById("rotateZvalue").innerHTML = sliderZ.value.toString();
        });
        sliderY.oninput = (function () {
            requestAnimationFrame(drawSceneAll);
            document.getElementById("rotateXvalue").innerHTML = sliderX.value.toString();
            document.getElementById("rotateYvalue").innerHTML = sliderY.value.toString();
            document.getElementById("rotateZvalue").innerHTML = sliderZ.value.toString();
        });
        sliderZ.oninput = (function () {
            requestAnimationFrame(drawSceneAll);
            document.getElementById("rotateXvalue").innerHTML = sliderX.value.toString();
            document.getElementById("rotateYvalue").innerHTML = sliderY.value.toString();
            document.getElementById("rotateZvalue").innerHTML = sliderZ.value.toString();
        });
        sliderE.oninput = (function () {
            requestAnimationFrame(drawSceneAll);
            document.getElementById("rotateXvalue").innerHTML = sliderX.value.toString();
            document.getElementById("rotateYvalue").innerHTML = sliderY.value.toString();
            document.getElementById("rotateZvalue").innerHTML = sliderZ.value.toString();
        });
    }
}
//
// initWebGL
//
// Initialize WebGL, returning the GL context or null if
// WebGL isn't available or could not be initialized.
//
function initWebGL() {
    gl = null;

    try {
        gl = canvas.getContext("experimental-webgl", {premultipliedalpha: false});
    }
    catch (e) {
    }

    // If we don't have a GL context, give up now

    if (!gl) {
        alert("Unable to initialize WebGL. Your browser may not support it.");
    }
}

//
// initBuffers
//
// Initialize the buffers we'll need. For this demo, we just have
// one object -- a simple two-dimensional cube.
//
function initBuffers() {

    // Create a buffer for the cube's vertices.

    cubeVerticesBuffer = gl.createBuffer();

    // Select the cubeVerticesBuffer as the one to apply vertex
    // operations to from here out.

    gl.bindBuffer(gl.ARRAY_BUFFER, cubeVerticesBuffer);

    // Now pass the list of vertices into WebGL to build the shape. We
    // do this by creating a Float32Array from the JavaScript array,
    // then use it to fill the current vertex buffer.

    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(figure.vertices), gl.STATIC_DRAW);

    // Now set up the colors for the faces. We'll use solid colors
    // for each face.

    // Convert the array of colors into a table for all the vertices.

    normalBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, normalBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(figure.normals), gl.STATIC_DRAW);

    cubeVerticesColorBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, cubeVerticesColorBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(figure.generatedColors), gl.STATIC_DRAW);

    // Build the element array buffer; this specifies the indices
    // into the vertex array for each face's vertices.

    cubeVerticesIndexBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, cubeVerticesIndexBuffer);

    // This array defines each face as two triangles, using the
    // indices into the vertex array to specify each triangle's
    // position.


    // Now send the element array to GL

    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER,
        new Uint16Array(figure.cubeVertexIndices), gl.STATIC_DRAW);
}


//
// initBuffers
//
// Initialize the buffers we'll need. For this demo, we just have
// one object -- a simple two-dimensional cube.
//
function initBuffersAll() {
    console.log("Kviečiamas initBuffersAll()");
    var i;
    for (i = 0; i < figures.length; i++) {
        var f = figures[i];

        // Create a buffer for the cube's vertices.

        f.cubeVerticesBuffer = gl.createBuffer();

        // Select the cubeVerticesBuffer as the one to apply vertex
        // operations to from here out.

        gl.bindBuffer(gl.ARRAY_BUFFER, f.cubeVerticesBuffer);

        // Now pass the list of vertices into WebGL to build the shape. We
        // do this by creating a Float32Array from the JavaScript array,
        // then use it to fill the current vertex buffer.

        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(f.vertices), gl.STATIC_DRAW);

        // Now set up the colors for the faces. We'll use solid colors
        // for each face.

        // Convert the array of colors into a table for all the vertices.


        f.cubeVerticesColorBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, f.cubeVerticesColorBuffer);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(f.generatedColors), gl.STATIC_DRAW);

        f.normalBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, f.normalBuffer);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(f.normals), gl.STATIC_DRAW);

        // Build the element array buffer; this specifies the indices
        // into the vertex array for each face's vertices.

        f.cubeVerticesIndexBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, f.cubeVerticesIndexBuffer);

        // This array defines each face as two triangles, using the
        // indices into the vertex array to specify each triangle's
        // position.


        // Now send the element array to GL

        gl.bufferData(gl.ELEMENT_ARRAY_BUFFER,
            new Uint16Array(f.cubeVertexIndices), gl.STATIC_DRAW);


        if (f.edges) {
            f.pointsPositionBuffer = gl.createBuffer();
            gl.bindBuffer(gl.ARRAY_BUFFER, f.pointsPositionBuffer);

            gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(f.edges), gl.STATIC_DRAW);
        }

        if (f.texIndeces) {
            f.texVerticesBuffer = gl.createBuffer();
            gl.bindBuffer(gl.ARRAY_BUFFER, f.texVerticesBuffer);
            gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(f.vertices), gl.STATIC_DRAW);

            f.texVerticesTextureCoordBuffer = gl.createBuffer();
            gl.bindBuffer(gl.ARRAY_BUFFER, f.texVerticesTextureCoordBuffer);
            gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(f.texMap),
                gl.STATIC_DRAW);

            f.texVerticesIndexBuffer = gl.createBuffer();
            gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, f.texVerticesIndexBuffer);
            gl.bufferData(gl.ELEMENT_ARRAY_BUFFER,
                new Uint16Array(f.texIndeces), gl.STATIC_DRAW);
        }
    }

}

function drawScene() {
    cubeRotationX = sliderX.value;
    document.getElementById("rotateXvalue").innerHTML = sliderX.value.toString();
    cubeRotationY = sliderY.value;
    document.getElementById("rotateYvalue").innerHTML = sliderY.value.toString();
    cubeRotationZ = sliderZ.value;
    document.getElementById("rotateZvalue").innerHTML = sliderZ.value.toString();
    // Clear the canvas before we start drawing on it.

    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    // Establish the perspective with which we want to view the
    // scene. Our field of view is 45 degrees, with a width/height
    // ratio of 640:480, and we only want to see objects between 0.1 units
    // and 100 units away from the camera.

    perspectiveMatrix = makePerspective(45, 640.0 / 480.0, 0.1, 100.0);

    // Set the drawing position to the "identity" point, which is
    // the center of the scene.

    loadIdentity();

    // Now move the drawing position a bit to where we want to start
    // drawing the cube.

    mvTranslate([-0.0, 0.0, -6.0]);
    mvScale([1.0, 1.0, 1.0]);

    // Save the current matrix, then rotate before we draw.

    mvPushMatrix();
    mvRotate(cubeRotationX, [1, 0, 0]);
    mvRotate(cubeRotationY, [0, 1, 0]);
    mvRotate(cubeRotationZ, [0, 0, 1]);
    mvTranslate([cubeXOffset, cubeYOffset, cubeZOffset]);

    // Draw the cube by binding the array buffer to the cube's vertices
    // array, setting attributes, and pushing it to GL.

    gl.bindBuffer(gl.ARRAY_BUFFER, cubeVerticesBuffer);
    gl.vertexAttribPointer(vertexPositionAttribute, 3, gl.FLOAT, false, 0, 0);

    // Set the colors attribute for the vertices.

    gl.bindBuffer(gl.ARRAY_BUFFER, cubeVerticesColorBuffer);
    gl.vertexAttribPointer(vertexColorAttribute, 4, gl.FLOAT, false, 0, 0);

    // Draw the cube.

    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, cubeVerticesIndexBuffer);
    setMatrixUniforms(shaderProgram);

    var index = gl.getUniformLocation(shaderProgram, "lightPos");
    gl.uniform3fv(index, lightPos);
    index = gl.getUniformLocation(shaderProgram, "ambientColor");
    gl.uniform3fv(index, ambientColor);
    index = gl.getUniformLocation(shaderProgram, "diffuseColor");
    gl.uniform3fv(index, diffuseColor);
    index = gl.getUniformLocation(shaderProgram, "specColor");
    gl.uniform3fv(index, specColor);
    index = gl.getUniformLocation(shaderProgram, "shininess");
    gl.uniform1f(index, shininess);
    index = gl.getUniformLocation(shaderProgram, "screenGamma");
    gl.uniform1f(index, screenGamma);

    gl.bindBuffer(gl.ARRAY_BUFFER, normalBuffer);
    gl.vertexAttribPointer(textureNormalAttribute, 3, gl.FLOAT, false, 0, 0);

    gl.drawElements(gl.TRIANGLES, figure.cubeVertexIndices.length, gl.UNSIGNED_SHORT, 0);

    // Restore the original matrix

    mvPopMatrix();

    // Update the rotation for the next draw, if it's time to do so.
}

//
// drawScene
//
// Draw the scene.
//
function drawSceneAll() {
    cubeRotationX = sliderX.value;
    document.getElementById("rotateXvalue").innerHTML = sliderX.value.toString();
    cubeRotationY = sliderY.value;
    document.getElementById("rotateYvalue").innerHTML = sliderY.value.toString();
    cubeRotationZ = sliderZ.value;
    document.getElementById("rotateZvalue").innerHTML = sliderZ.value.toString();
    // Clear the canvas before we start drawing on it.

    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    // Establish the perspective with which we want to view the
    // scene. Our field of view is 45 degrees, with a width/height
    // ratio of 640:480, and we only want to see objects between 0.1 units
    // and 100 units away from the camera.

    perspectiveMatrix = makePerspective(45, 640.0 / 480.0, 0.1, 100.0);

    // Set the drawing position to the "identity" point, which is
    // the center of the scene.

    loadIdentity();

    // Now move the drawing position a bit to where we want to start
    // drawing the cube.

    mvTranslate([-0.0, 0.0, -6.0]);
    mvScale([1.0, 1.0, 1.0]);
    mvRotate(cubeRotationX, [1, 0, 0]);
    mvRotate(cubeRotationY, [0, 1, 0]);
    mvRotate(cubeRotationZ, [0, 0, 1]);

    // Save the current matrix, then rotate before we draw.


    for (var i = 0; i < figures.length; i++) {
        var f = figures[i];
        mvPushMatrix();
        mvTranslate(f.t);
        mvTranslate(vec3mul(f.e, sliderE.value));
        mvRotate(f.r[0], [1, 0, 0]);
        mvRotate(f.r[1], [0, 1, 0]);
        mvRotate(f.r[2], [0, 0, 1]);
        mvTranslate([cubeXOffset, cubeYOffset, cubeZOffset]);

        // Draw the cube by binding the array buffer to the cube's vertices
        // array, setting attributes, and pushing it to GL.


        if(document.getElementById("bricks").checked) {
            gl.useProgram(shaderProgram);

            gl.bindBuffer(gl.ARRAY_BUFFER, f.cubeVerticesBuffer);
            gl.vertexAttribPointer(vertexPositionAttribute, 3, gl.FLOAT, false, 0, 0);

            // Set the colors attribute for the vertices.

            gl.bindBuffer(gl.ARRAY_BUFFER, f.cubeVerticesColorBuffer);
            gl.vertexAttribPointer(vertexColorAttribute, 4, gl.FLOAT, false, 0, 0);

            // Draw the cube.

            gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, f.cubeVerticesIndexBuffer);
            setMatrixUniforms(shaderProgram);
            var index = gl.getUniformLocation(shaderProgram, "lightPos");
            gl.uniform3fv(index, lightPos);
            index = gl.getUniformLocation(shaderProgram, "ambientColor");
            gl.uniform3fv(index, ambientColor);
            index = gl.getUniformLocation(shaderProgram, "diffuseColor");
            gl.uniform3fv(index, diffuseColor);
            index = gl.getUniformLocation(shaderProgram, "specColor");
            gl.uniform3fv(index, specColor);
            index = gl.getUniformLocation(shaderProgram, "shininess");
            gl.uniform1f(index, shininess);
            index = gl.getUniformLocation(shaderProgram, "screenGamma");
            gl.uniform1f(index, screenGamma);

            gl.bindBuffer(gl.ARRAY_BUFFER, f.normalBuffer);
            gl.vertexAttribPointer(textureNormalAttribute, 3, gl.FLOAT, false, 0, 0);
            gl.drawElements(gl.TRIANGLES, f.cubeVertexIndices.length, gl.UNSIGNED_SHORT, 0);
        }

        // Restore the original matrix

        if (f.texIndeces && document.getElementById("textures").checked) {

            // Draw the cube by binding the array buffer to the cube's vertices
            // array, setting attributes, and pushing it to GL.
            gl.useProgram(textureProgram);

            gl.bindBuffer(gl.ARRAY_BUFFER, f.texVerticesBuffer);
            gl.vertexAttribPointer(tvertexPositionAttribute, 3, gl.FLOAT, false, 0, 0);

            // Set the texture coordinates attribute for the vertices.

            gl.bindBuffer(gl.ARRAY_BUFFER, f.texVerticesTextureCoordBuffer);
            gl.vertexAttribPointer(textureCoordAttribute, 2, gl.FLOAT, false, 0, 0);

            // Specify the texture to map onto the faces.

            gl.activeTexture(gl.TEXTURE0);
            gl.bindTexture(gl.TEXTURE_2D, cubeTexture);
            gl.uniform1i(gl.getUniformLocation(textureProgram, "uSampler"), 0);

            setMatrixUniforms(textureProgram);
            index = gl.getUniformLocation(textureProgram, "lightPos");
            gl.uniform3fv(index, lightPos);
            index = gl.getUniformLocation(textureProgram, "ambientColor");
            gl.uniform3fv(index, ambientColor);
            index = gl.getUniformLocation(textureProgram, "diffuseColor");
            gl.uniform3fv(index, diffuseColor);
            index = gl.getUniformLocation(textureProgram, "specColor");
            gl.uniform3fv(index, specColor);
            index = gl.getUniformLocation(textureProgram, "shininess");
            gl.uniform1f(index, shininess);
            index = gl.getUniformLocation(textureProgram, "screenGamma");
            gl.uniform1f(index, screenGamma);

            gl.bindBuffer(gl.ARRAY_BUFFER, f.normalBuffer);
            gl.vertexAttribPointer(textureNormalAttribute, 3, gl.FLOAT, false, 0, 0);

            // Draw the cube.

            gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, f.texVerticesIndexBuffer);
            gl.drawElements(gl.TRIANGLES, f.texIndeces.length, gl.UNSIGNED_SHORT, 0);
        }
        mvPopMatrix();

        // Update the rotation for the next draw, if it's time to do so.
    }

    if (document.getElementById("lines").checked) {

        for (var i = 0; i < figures.length; i++) {
            var f = figures[i];
            if (f.edges) {
                mvPushMatrix();
                mvTranslate(f.t);
                mvTranslate(vec3mul(f.e, sliderE.value));
                mvRotate(f.r[0], [1, 0, 0]);
                mvRotate(f.r[1], [0, 1, 0]);
                mvRotate(f.r[2], [0, 0, 1]);
                mvTranslate([cubeXOffset, cubeYOffset, cubeZOffset]);

                // Draw the cube by binding the array buffer to the cube's vertices
                // array, setting attributes, and pushing it to GL.

                // Restore the original matrix

                gl.useProgram(lineProgram);
                gl.bindBuffer(gl.ARRAY_BUFFER, f.pointsPositionBuffer);
                gl.vertexAttribPointer(lvertexPositionAttribute, 3, gl.FLOAT, false, 0, 0);
                setMatrixUniforms(lineProgram);
                gl.lineWidth(2.0);
                gl.drawArrays(gl.LINES, 0, f.edges.length / 3);

                mvPopMatrix();
            }

            // Update the rotation for the next draw, if it's time to do so.
        }

        //gl.disable(gl.BLEND);
        gl.useProgram(shaderProgram);
    }

}

//
// initShaders
//
// Initialize the shaders, so WebGL knows how to light our scene.
//
function initShaders() {
    if (!shaderProgram) {
        var fragmentShader = getShader(gl, "shader-fs");
        var vertexShader = getShader(gl, "shader-vs");

        // Create the shader program

        shaderProgram = gl.createProgram();
        gl.attachShader(shaderProgram, vertexShader);
        gl.attachShader(shaderProgram, fragmentShader);
        gl.linkProgram(shaderProgram);

        // If creating the shader program failed, alert

        if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
            alert("Unable to initialize the shader program.");
        }

        gl.useProgram(shaderProgram);

        vertexPositionAttribute = gl.getAttribLocation(shaderProgram, "aVertexPosition");
        gl.enableVertexAttribArray(vertexPositionAttribute);

        vertexColorAttribute = gl.getAttribLocation(shaderProgram, "aVertexColor");
        gl.enableVertexAttribArray(vertexColorAttribute);
    }

    if (!lineProgram) {
        var lFragmentShader = getShader(gl, "shader-fs-line");
        var lVertexShader = getShader(gl, "shader-vs-line");

        lineProgram = gl.createProgram();
        gl.attachShader(lineProgram, lVertexShader);
        gl.attachShader(lineProgram, lFragmentShader);
        gl.linkProgram(lineProgram);

        if (!gl.getProgramParameter(lineProgram, gl.LINK_STATUS)) {
            alert("Unable to initialize the shader program.");
        }

        gl.useProgram(lineProgram);

        lvertexPositionAttribute = gl.getAttribLocation(lineProgram, "aVertexPosition");
        gl.enableVertexAttribArray(lvertexPositionAttribute);
    }

    if (!textureProgram) {

        var tfragmentShader = getShader(gl, "shader-fs-texture");
        var tvertexShader = getShader(gl, "shader-vs-texture");

        // Create the shader program

        textureProgram = gl.createProgram();
        gl.attachShader(textureProgram, tvertexShader);
        gl.attachShader(textureProgram, tfragmentShader);
        gl.linkProgram(textureProgram);

        // If creating the shader program failed, alert

        if (!gl.getProgramParameter(textureProgram, gl.LINK_STATUS)) {
            alert("Unable to initialize the shader program.");
        }

        gl.useProgram(textureProgram);

        tvertexPositionAttribute = gl.getAttribLocation(textureProgram, "aVertexPosition");
        gl.enableVertexAttribArray(tvertexPositionAttribute);

        textureCoordAttribute = gl.getAttribLocation(textureProgram, "aTextureCoord");
        gl.enableVertexAttribArray(textureCoordAttribute);

        textureNormalAttribute = gl.getAttribLocation(textureProgram, "aNormal");
        gl.enableVertexAttribArray(textureNormalAttribute);
    }

    gl.useProgram(shaderProgram);
}

//
// getShader
//
// Loads a shader program by scouring the current document,
// looking for a script with the specified ID.
//
function getShader(gl, id) {
    var shaderScript = document.getElementById(id);

    // Didn't find an element with the specified ID; abort.

    if (!shaderScript) {
        return null;
    }

    // Walk through the source element's children, building the
    // shader source string.

    var theSource = "";
    var currentChild = shaderScript.firstChild;

    while (currentChild) {
        if (currentChild.nodeType == 3) {
            theSource += currentChild.textContent;
        }

        currentChild = currentChild.nextSibling;
    }

    // Now figure out what type of shader script we have,
    // based on its MIME type.

    var shader;

    if (shaderScript.type == "x-shader/x-fragment") {
        shader = gl.createShader(gl.FRAGMENT_SHADER);
    } else if (shaderScript.type == "x-shader/x-vertex") {
        shader = gl.createShader(gl.VERTEX_SHADER);
    } else {
        return null;  // Unknown shader type
    }

    // Send the source to the shader object

    gl.shaderSource(shader, theSource);

    // Compile the shader program

    gl.compileShader(shader);

    // See if it compiled successfully

    if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
        alert("An error occurred compiling the shaders: " + gl.getShaderInfoLog(shader));
        return null;
    }

    return shader;
}

//
// Matrix utility functions
//

function loadIdentity() {
    mvMatrix = Matrix.I(4);
}

function multMatrix(m) {
    mvMatrix = mvMatrix.x(m);
}

function mvTranslate(v) {
    multMatrix(Matrix.Translation($V([v[0], v[1], v[2]])).ensure4x4());
}

function mvScale(v) {
    multMatrix(Matrix.Scale($V([v[0], v[1], v[2]])).ensure4x4());
}

function setMatrixUniforms(program) {
    var pUniform = gl.getUniformLocation(program, "uPMatrix");
    gl.uniformMatrix4fv(pUniform, false, new Float32Array(perspectiveMatrix.flatten()));

    var mvUniform = gl.getUniformLocation(program, "uMVMatrix");
    gl.uniformMatrix4fv(mvUniform, false, new Float32Array(mvMatrix.flatten()));

    var normalUniform = gl.getUniformLocation(program, "uNormalMatrix");
    normalMatrix = mvMatrix.inverse();
    if(!normalMatrix) {
        console.log("Negalima apskaičiuoti atvirkštinės matricos");
        console.log(mvMatrix.elements[0]);
        console.log(mvMatrix.elements[1]);
        console.log(mvMatrix.elements[2]);
        console.log(mvMatrix.elements[3]);
    }
    normalMatrix = normalMatrix.transpose();
    gl.uniformMatrix4fv(normalUniform, false, new Float32Array(normalMatrix.flatten()));
}

var mvMatrixStack = [];

function mvPushMatrix(m) {
    if (m) {
        mvMatrixStack.push(m.dup());
        mvMatrix = m.dup();
    } else {
        mvMatrixStack.push(mvMatrix.dup());
    }
}

function mvPopMatrix() {
    if (!mvMatrixStack.length) {
        throw("Can't pop from an empty matrix stack.");
    }

    mvMatrix = mvMatrixStack.pop();
    return mvMatrix;
}

function mvRotate(angle, v) {
    var inRadians = angle * Math.PI / 180.0;

    var m = Matrix.Rotation(inRadians, $V([v[0], v[1], v[2]])).ensure4x4();
    multMatrix(m);
}

function initTextures() {
    cubeImage = new Image();
    cubeImage.onload = function () {
        cubeTexture = gl.createTexture();
        handleTextureLoaded(cubeImage, cubeTexture);
    }
    cubeImage.src = "dalius.png";
}

function handleTextureLoaded(image, texture) {
    console.log("handleTextureLoaded, image = " + image);
    gl.bindTexture(gl.TEXTURE_2D, texture);
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA,
        gl.UNSIGNED_BYTE, image);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_NEAREST);
    gl.generateMipmap(gl.TEXTURE_2D);
    gl.bindTexture(gl.TEXTURE_2D, null);
}